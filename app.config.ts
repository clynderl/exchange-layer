export default defineAppConfig({
  myLayer: {
    name: "exchange-layer"
  }
})

declare module '@nuxt/schema' {
  interface AppConfigInput {
    myLayer?: {
      /** Project name */
      name?: string
    }
  }
}
