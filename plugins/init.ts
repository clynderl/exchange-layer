export default defineNuxtPlugin(async (nuxtApp) => {
  if (nuxtApp.payload.error) {
    return {};
  }
  const {fetchDirections, fetchReserves} = useExchange();

  await fetchDirections();
  await fetchReserves();
  return {};
});
