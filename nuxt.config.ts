// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    '@vueuse/nuxt',
    '@pinia/nuxt',
    '@nuxtjs/color-mode'
  ],

  pinia: {
    autoImports: [
      'defineStore', ['defineStore', 'definePiniaStore'],
    ],
  },
})
