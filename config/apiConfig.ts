import calculator from '~/config/api/calculator';
import users from '~/config/api/users';
import news from '~/config/api/news';
import claim from '~/config/api/claim';

const apiConfig = {
  ...calculator,
  ...users,
  ...news,
  ...claim,
};

export default apiConfig;
